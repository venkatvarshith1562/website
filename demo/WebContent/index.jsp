<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js" integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js" integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous"></script>
<link href="styles.css" rel="stylesheet">
<meta charset="ISO-8859-1">
<title>Demo-project</title>
</head>
<body>
<div class="container-fluid">
  <nav class="navbar navbar-expand-lg navbar-light">
          <a href="" class="navbar-brand" id="brand">Project</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto">
              <li class="nav-item">
                <a class="nav-link active" id="link" href="index.jsp">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="link" href="userhistory.jsp">UserHistory</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="link" href="uploaddataset.jsp">UploadDataset</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="link" href="analyzedata.jsp">Analyzeddata</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="link" href="graph.jsp">Graph</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="link" href="about.jsp">About</a>
              </li>
            </ul>
          </div>
        </nav>  
        <hr style="color:black;"   />
        <div class="head">
        	<br>
        	<h2>Detecting Malicious Social Bots Based on ClickStream Sequence <div>
        		-Venkat
        	</div></h2>
        	<br>
     	   	<h4>Abstract:</h4>
        	Social bots are social accounts controlled by automated programs that can perform corresponding operations based on a set of processes. The increasing use of mobile devices i.e., Android and iOS devices also contributes to an increase in the frequency and nature of user interaction via social networks. It is evident from the significant volume, velocity and variety of data generated from large online social network user base. Social bots have been widely deployed to enhance the quality and efficiency of collecting and analysing data from social network services. For example, the social bot SF QuakeBot is designed to generate earthquake reports in the San Francisco Bay, it can analyse earthquake related information in social networks in real-time. In online social networks, automatic social bots cannot represent the real desires and intentions of normal human beings, so they are usually looked upon malicious ones. For example, some fake social bots accounts created to imitate the profile of a normal user, steal user data and compromise their privacy, disseminate malicious or fake information, malicious comment, promote or advance certain political or ideology agenda and propaganda, and influence the stock market and other societal and economical markets. Such activities can adversely impact the security and stability of social networking platforms. User behaviour is the most direct manifestation of user intent, as different users have different habits, preferences, and online behaviour (e.g., the way one clicks or types, as well as the speed of typing). In other words, we may be able to mine and analyse information hidden in user's online behaviour to problem and identify different users. User behaviour is dynamic, and its environment is constantly changing i.e., external observable environment (e.g., environment and behaviour) of application context and the hidden environment in user information. In order to distinguish social bots from normal users accurately, detect malicious social bots, and reduce the harm of malicious social bots, we need to acquire and analyse social situation of user behaviour and compare and understand the differences of malicious social bots and normal users in dynamic behaviour. Our aim is to detect malicious social bots on social network platforms in Realtime, by (1) proposing the transition probability features between user clickstreams based on the social situation analytics; and (2) designing an algorithm for detecting malicious social bots based on spatiotemporal features.
        </div>
 </div>
 
 
</body>
</html>